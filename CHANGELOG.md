dune-functions
========

This is the development version of dune-functions leading up to version 2.6.

It is supposed to be compatible with the Dune core modules of the same version.

Changes
=======

- Added an implementation of a Rannacher-Turek basis
- Add a set of unit tests for bases
- Extend the documentation of these bases

dune-functions 2.5
------------

TODO...



